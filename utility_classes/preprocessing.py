import cv2
import numpy as np

def odt(image, threshold=0.2):
    I = np.power(10, -threshold)*255
    I = np.round(I).astype(np.uint8)
    mask = (image >= I).astype(np.uint8)
    return mask*I
