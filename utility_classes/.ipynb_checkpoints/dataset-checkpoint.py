import torch
import torchvision
from torch.utils import data
from PIL import Image
import numpy as np
import pandas as pd

# Note: Dataset class uses albumentations for transforming the data

class Dataset(data.Dataset):
    
    def __init__(self, img_path, labels, transforms=None):
        super().__init__()
        self.img_path = img_path
        self.labels = labels
        self.transforms = transforms

    def __getitem__(self, index):
        X = Image.open(self.img_path + "/" + self.labels.loc[index, 'id'] + ".tif")
        y = self.labels.loc[index, 'id']

        if self.transforms is not None:
            X = np.array(X)
            X = self.transforms(image=X)['image']

        return X,y

    def __len__(self):
        return self.labels.shape[0]
