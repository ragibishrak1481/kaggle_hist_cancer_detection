import torch
import datetime

class TorchModelWriter:
    '''two types of write:
    1. checkpoint for saving in train loop
    2. best for saving the best model
    ''' 
    
    file_suffix = None
    def __init__(self, path, write_type="checkpoint"):
        self.write_type = write_type
        self.path = path
        x = datetime.datetime.now()
        self.file_suffix = "{}-{}_{}-{}-{}".format(x.hour, x.minute, x.day, x.month, x.year)
    
    def write(self, model, optimizer=None, epoch=None, loss=None, scheduler=None):

        if self.write_type == "checkpoint":
            # print("saving checkpoint for epoch {}...".format(epoch), end='')
            torch.save({"epoch": epoch,
                        "model_state_dict": model.state_dict(),
                        "optimizer_state_dict": optimizer.state_dict(),
                        "loss": loss,
                        "scheduler": scheduler.state_dict()},
                        self.path + "/" + model.__class__.__name__ + "_" + self.file_suffix + "_chkpt.pt")
            # print(" saved.")
        
        elif self.write_type == "best":
            # print("saving best performing model after epoch {}...".format(epoch), end='')
            torch.save(model.state_dict(), self.path+"/" + model.__class__.__name__ + "_" + self.file_suffix + "_best.pt")
            # print(" saved.")
        else:
            print("invalid save option")