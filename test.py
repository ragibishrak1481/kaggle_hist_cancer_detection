# Import packages and setup environment
import numpy as np
import pandas as pd

import torch
from torch import optim, nn
from torch.utils import data
from torchvision import transforms

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
import albumentations
from albumentations import torch as AT

from utility_classes.dataset import Dataset
from custom_models import model_1, model_2, model_3
from utility_classes.checkpoint_utility import TorchModelWriter
from tensorboardX import SummaryWriter
from torchsummary import summary
from tqdm import tqdm

import datetime
import warnings
warnings.filterwarnings("ignore")


def main():
    #Define Hyperparams and setup constants
    NUM_WORKERS = 6 
    BATCH_SIZE = 512
    INPUT_SIZE = (3, 128, 128)
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print(device) 
    
    # Define dataloader: 
    test_labels = pd.read_csv("./sample_submission.csv")
    
    xforms = albumentations.Compose([albumentations.CenterCrop(32, 32),
                                    albumentations.Resize(INPUT_SIZE[1], INPUT_SIZE[2]),
                                    albumentations.Normalize(),
                                    AT.ToTensor()])
    
    dataset = Dataset(img_path="./test", labels=test_labels, transforms=xforms)
    
    dataloader = data.DataLoader(dataset=dataset,
                                 shuffle=False,
                                 batch_size=BATCH_SIZE,
                                 num_workers=NUM_WORKERS)
    
    
    # Load net 
    net = model_1.Model1().to(device)
    net.load_state_dict(torch.load('./checkpoints/Model1_best.pt'))
    summary(net, INPUT_SIZE, batch_size=BATCH_SIZE)

    # prediction loop
    predictions = np.empty([0])
    net.eval()
    with torch.no_grad():
        for image, label in tqdm(dataloader):
            image = image.to(device)
            y_pred = net(image)
            predictions = np.append(predictions, y_pred.detach().cpu().view(-1).numpy())

    # end script
    test_labels['label'] = predictions
    test_labels.to_csv('./submission.csv', index=False)
    torch.cuda.empty_cache()
    
if __name__=="__main__":
    main()
