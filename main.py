# Import packages and setup environment
import numpy as np
import pandas as pd

import torch
from torch import optim, nn
from torch.utils import data
from torchvision import transforms

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
import albumentations
from albumentations import torch as AT

from utility_classes.dataset import Dataset
from custom_models import model_1, model_2, model_3
from utility_classes.checkpoint_utility import TorchModelWriter
from tensorboardX import SummaryWriter
from torchsummary import summary
from tqdm import tqdm

import datetime
import warnings
warnings.filterwarnings("ignore")


def main():
    #Define Hyperparams and setup constants
    NUM_WORKERS = 6 
    BATCH_SIZE = 512
    EPOCHS = 500
    WEIGHT_DECAY = 0.1
    LEARNING_RATE= 0.001
    MOMENTUM = 0.9
    INPUT_SIZE = (3, 32, 32)
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print(device) 
    
    # Define dataloader: train and cross_val
    train_labels = pd.read_csv("./train.csv")
    cross_val_labels = pd.read_csv("./validation.csv")
    
    train_xforms = albumentations.Compose([albumentations.CenterCrop(32, 32),
                                           albumentations.Resize(INPUT_SIZE[1], INPUT_SIZE[2]),
                                           albumentations.HorizontalFlip(),
                                           albumentations.ShiftScaleRotate(rotate_limit=15, scale_limit=0.10),
                                           albumentations.Normalize(),
                                           AT.ToTensor()])
    
    cross_val_xforms = albumentations.Compose([albumentations.CenterCrop(32, 32),
                                              albumentations.Resize(INPUT_SIZE[1], INPUT_SIZE[2]),
                                              albumentations.Normalize(),
                                              AT.ToTensor()])
    
    trainset = Dataset(img_path="./train", labels=train_labels, transforms=train_xforms)
    valset = Dataset(img_path="./train", labels=cross_val_labels, transforms=cross_val_xforms)
    
    trainloader = data.DataLoader(dataset=trainset,
                                 shuffle=True,
                                 batch_size=BATCH_SIZE,
                                 num_workers=NUM_WORKERS)
    
    valloader = data.DataLoader(dataset=valset,
                                 shuffle=False,
                                 batch_size=BATCH_SIZE,
                                 num_workers=NUM_WORKERS)
    
    
    # Initialize net, optimizer, scheduler & loss function
    net = model_3.Model3().to(device)
    optimizer = optim.SGD(net.parameters(), lr=LEARNING_RATE, momentum=MOMENTUM, weight_decay=WEIGHT_DECAY)
    loss_func = nn.BCEWithLogitsLoss()
    lr_scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[10, 20, 100])
    summary(net, INPUT_SIZE, batch_size=BATCH_SIZE)
    
    
    # Helper functions and classes
    checkpoint_writer = TorchModelWriter("./checkpoints/", write_type="checkpoint")
    best_model_writer = TorchModelWriter("./checkpoints/", write_type="best")
    
    x = datetime.datetime.now()
    summarywriter_name = "run_{}-{}_{}-{}-{}".format(x.hour, x.minute, x.day, x.month, x.year)
    writer = SummaryWriter("./log/runs/{}/{}".format(net.__class__.__name__, summarywriter_name))
    
    # load model
    
    
    # training loop
    total_iter = 0
    train_loss_sum = 0
    prev_val_best_roc_auc_score = None
    val_loss_sum = 0
    val_auc_score_sum = 0
    val_labels = np.empty([0])
    val_preds = np.empty([0])
    train_labels_target = np.empty([0])
    train_labels_pred = np.empty([0])
    
    for epoch in range(EPOCHS):
        net.train()
        lr_scheduler.step()
        print("Epoch {}".format(epoch))
        for image, labels in tqdm(trainloader, desc="Training progress: "):
            total_iter += 1
            
            image = image.to(device)
            labels = labels.view(-1,1).to(device)
            
            optimizer.zero_grad()
            
            y_pred = net(image)
            loss = loss_func(y_pred, labels)
            loss.backward()
            optimizer.step()
            
            train_loss_sum += loss.item()
            
            writer.add_scalar("data/batch_loss", loss.item(), total_iter)
            
            train_labels_target = np.append(train_labels_target, labels.detach().cpu().view(-1).numpy())
            train_labels_pred = np.append(train_labels_pred, y_pred.detach().cpu().view(-1).numpy())
            
        # save training checkpoint after each epoch
        checkpoint_writer.write(epoch=epoch, loss=loss, model=net, optimizer=optimizer, scheduler=lr_scheduler)
        
        net.eval()
        with torch.no_grad():
            for image, labels in tqdm(valloader, desc="Validation progress: "):
                image = image.to(device)
                labels = labels.view(-1,1).to(device)
                
                y_pred = net(image)
                loss = loss_func(y_pred, labels)
                val_loss_sum += loss.item()
                
                val_labels = np.append(val_labels, labels.detach().cpu().view(-1).numpy())
                val_preds = np.append(val_preds, y_pred.detach().cpu().view(-1).numpy())
                
            val_auc_score = roc_auc_score(val_labels, val_preds)
           
            if (prev_val_best_roc_auc_score is None):
                prev_val_best_roc_auc_score = val_auc_score
                writer.add_scalar("data/best_auc_score", val_auc_score, epoch)
                print("Saving best model")
                best_model_writer.write(model=net, epoch=epoch)
                
            if (prev_val_best_roc_auc_score < val_auc_score):
                writer.add_scalar("data/best_auc_score", val_auc_score, epoch)
                best_model_writer.write(model=net, epoch=epoch)
                print("Saving best model")
                prev_val_best_roc_auc_score = val_auc_score
                 
            writer.add_scalar("data/validation_auc_score", val_auc_score, epoch)
            writer.add_scalar("data/training_auc_score", roc_auc_score(train_labels_target, train_labels_pred), epoch)
            writer.add_scalar("data/validation_loss", val_loss_sum/len(valloader), epoch)
            writer.add_scalar("data/train_loss", train_loss_sum/len(trainloader), epoch)
            
            train_loss_sum = 0
            val_loss_sum = 0
                
            val_labels = np.empty([0])
            val_preds = np.empty([0])
            train_labels_target = np.empty([0])
            train_labels_pred = np.empty([0])
        
    
    # end script
    writer.close()
    torch.cuda.empty_cache()
    
if __name__=="__main__":
    main()
