import torch
from torch import nn
import torchvision.models as models


class ModelResNet34Base(nn.Module):
    def __init__(self):
        super(ModelResNet34Base, self).__init__()
        
        base = models.resnet34(pretrained=True)
        self.feature = nn.Sequential(*list(base.children())[:-2])
        self.classifier = nn.Sequential(
                nn.Linear(512*2, 512),
                nn.ReLU(),
                nn.BatchNorm1d(512),
                nn.Dropout(p=0.5),
                nn.Linear(512, 1),)

    def forward(self, x):
        x = self.feature(x)
        x = x.view(-1, 512*2)
        x = self.classifier(x)
        if not self.training:
            x = torch.sigmoid(x)
        return x

