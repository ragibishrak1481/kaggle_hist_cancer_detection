import torch
import torchvision.models as models
from torch import nn

class ModelVGThree(nn.Module):
    def __init__(self):
        super(ModelVGThree, self).__init__()
        vgg11 = models.vgg11_bn(pretrained=True)
        vgg13 = models.vgg13_bn(pretrained=True)
        vgg16 = models.vgg16_bn(pretrained=True)

        self.feat1 = vgg11.features
        self.feat2 = vgg13.features
        self.feat3 = vgg16.features

        self.fc1_1 = nn.Sequential(nn.Linear(512, 100, bias=True),
                nn.ReLU(), nn.Dropout(p=0.5))

        self.fc1_2 = nn.Sequential(nn.Linear(512, 100, bias=True),
                nn.ReLU(), nn.Dropout(p=0.5))

        self.fc1_3 = nn.Sequential(nn.Linear(512, 100, bias=True),
                nn.ReLU(), nn.Dropout(p=0.5))

        self.fc2 = nn.Sequential(nn.Linear(300, 100, bias=True),
                nn.ReLU(), nn.Dropout(p=0.5))

        self.fc3 = nn.Linear(100, 1, bias=True)

    def forward(self, x):
        x1 = self.feat1(x)
        x2 = self.feat2(x)
        x3 = self.feat3(x)

        x1 = x1.view(-1, 512)
        x2 = x2.view(-1, 512)
        x3 = x3.view(-1, 512)
        
        x1 = self.fc1_1(x1)
        x2 = self.fc1_2(x2)
        x3 = self.fc1_3(x3)
        x = torch.cat((x1, x2, x3), 1)
        x = self.fc2(x)
        x = self.fc3(x)

        if not self.training:
            x = torch.sigmoid(x)
        return x
