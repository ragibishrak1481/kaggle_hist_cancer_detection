import torch
from torch import nn
import torchvision.models as models
import torch.nn.functional as F

class Model1(nn.Module):
    def __init__(self):
        super(Model1, self).__init__()
        self.convolutional_layer = nn.Sequential(
                nn.Conv2d(3, 32, kernel_size=5, stride=1, padding=0),
                nn.MaxPool2d(kernel_size=2),
                nn.ReLU(),
                nn.Conv2d(32, 32, kernel_size=5, stride=1, padding=0),
                nn.MaxPool2d(kernel_size=2, padding=1),
                nn.ReLU(),
                nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=0),
                nn.MaxPool2d(kernel_size=2),
                nn.ReLU(),
                nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=0),
                nn.ReLU(),
                )

        self.fc1 = nn.Linear(12*12*64, 2048)
        self.fc2 = nn.Linear(2048, 1024)
        self.fc3 = nn.Linear(1024, 1)

    def forward(self, x):
        x = self.convolutional_layer(x)
        x = x.view(-1, 12*12*64)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        if not self.training:
            x = torch.sigmoid(x)
        return x

